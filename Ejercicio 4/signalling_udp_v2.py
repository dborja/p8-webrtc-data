import asyncio
import socket
from aiortc import RTCSessionDescription
import json


class SignalingUDPServer:
    def _init_(self):
        self.clients = {}
        self.servers = {}
        self.UDP_IP = "127.0.0.1"
        self.UDP_PORT = 5005

    async def handle_incoming_messages(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.bind((self.UDP_IP, self.UDP_PORT))

        while True:
            data, addr = await self.loop.sock_recv(sock, 1024)
            message = json.loads(data.decode())
            await self.process_message(message, addr)

    async def process_message(self, message, addr):
        if message['type'] == 'register_client':
            self.clients[message['id']] = addr
            print(f"Client {message['id']} registered at {addr}")
        elif message['type'] == 'register_server':
            self.servers[message['id']] = addr
            print(f"Server {message['id']} registered at {addr}")
        elif message['type'] == 'offer':
            peer_id = message['id']
            if peer_id not in self.clients and peer_id not in self.servers:
                return
            if peer_id not in self.clients:
                self.clients[peer_id] = addr
            await self.forward_message(message, addr)
        elif message['type'] == 'answer':
            await self.forward_message(message, addr)

    async def forward_message(self, message, sender_addr):
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        for peer_id, peer_addr in self.clients.items():
            if peer_addr != sender_addr:
                await self.loop.sock_sendto(sock, json.dumps(message).encode(), peer_addr)
        for peer_id, peer_addr in self.servers.items():
            if peer_addr != sender_addr:
                await self.loop.sock_sendto(sock, json.dumps(message).encode(), peer_addr)

    async def start_server(self):
        self.loop = asyncio.get_running_loop()
        await self.handle_incoming_messages()


async def main():
    signaling_server = SignalingUDPServer()
    await signaling_server.start_server()

if __name__ == "__main__":
    asyncio.run(main())