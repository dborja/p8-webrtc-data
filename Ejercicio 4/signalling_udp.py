import asyncio
import json
import socket
from aiortc.contrib.signaling import *


class SignalingServer:

    def __int__(self):
        self.clients = {}
        self.servers = {}
        self.udp_ip = "127.0.0.1"
        self.udp_port = 5005

    async def on_data_received(self, signaling):

        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.bind((self.udp_ip, self.udp_port))
        data, addr = sock.recvfrom(1024)
        message_type = data.split()[0].decode()

        if message_type == 'REGISTER':
            self.register_entity(data.decode())
        elif message_type in ['offer', 'answer']:
            self.process_sdp_message(data.decode(), signaling)

    def register_entity(self, data):
        entity_type, address = data.split()[1:]
        ip, port = address.split(':')

        if entity_type == 'CLIENT':
            self.clients[ip] = int(port)
        elif entity_type == 'SERVER':
            self.servers[ip] = int(port)

    def process_sdp_message(self, data, signaling):
        sdp_message = json.loads(data)
        target_ip = sdp_message['target_ip']
        target_port = self.clients.get(target_ip) or self.servers.get(target_ip)

        if target_port:
            self.send_sdp_message(target_ip, target_port, sdp_message, signaling)

    def send_sdp_message(self, target_ip, target_port, sdp_message, signaling):
        signaling.send(target_ip, target_port, json.dumps(sdp_message).encode())


async def main():
    signaling = SignalingServer()
    await signaling.on_data_received()

if __name__ == "__manin__":
    asyncio.run(main())