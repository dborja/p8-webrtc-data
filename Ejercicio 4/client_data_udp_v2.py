import asyncio
from aiortc import RTCPeerConnection, RTCSessionDescription
import json
import socket

async def send_message_over_udp(message):
    UDP_IP = "127.0.0.1"
    UDP_PORT = 5005

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.sendto(message.encode(), (UDP_IP, UDP_PORT))

async def create_peer_connection():
    pc = RTCPeerConnection()

    @pc.on("datachannel")
    def on_datachannel(channel):
        @channel.on("message")
        async def on_message(message):
            print(f"Received message: {message}")

    channel = pc.createDataChannel("chat")
    return pc, channel

async def register_with_signaling():
    client_id = "client1"  # Replace with a unique client ID

    registration_message = {
        "type": "register_client",
        "id": client_id
    }
    await send_message_over_udp(json.dumps(registration_message))

async def send_offer():
    client_id = "client1"  # Replace with a unique client ID

    offer_message = {
        "type": "offer",
        "id": client_id,
        "content": "This is an offer message"
    }
    await send_message_over_udp(json.dumps(offer_message))

async def send_message(channel, message):
    await channel.send(message)

async def main():
    await register_with_signaling()

    pc, channel = await create_peer_connection()

    await send_offer()

    while True:
        message = input("Enter message to send: ")
        if message == "exit":
            break
        await send_message(channel, message)

if __name__ == "__main__":
    asyncio.run(main())