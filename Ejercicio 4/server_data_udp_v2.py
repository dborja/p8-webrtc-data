import asyncio
from aiortc import RTCPeerConnection, RTCSessionDescription
import json
import socket

async def send_message_over_udp(message):
    UDP_IP = "127.0.0.1"
    UDP_PORT = 5005

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.sendto(message.encode(), (UDP_IP, UDP_PORT))

async def create_peer_connection():
    pc = RTCPeerConnection()

    @pc.on("datachannel")
    def on_datachannel(channel):
        @channel.on("message")
        async def on_message(message):
            print(f"Received message: {message}")
            # Respond with an acknowledgment or process the message as needed

    return pc

async def register_with_signaling():
    server_id = "server1"  # Replace with a unique server ID

    registration_message = {
        "type": "register_server",
        "id": server_id
    }
    await send_message_over_udp(json.dumps(registration_message))

async def send_answer():
    server_id = "server1"  # Replace with a unique server ID

    answer_message = {
        "type": "answer",
        "id": server_id,
        "content": "This is an answer message"
    }
    await send_message_over_udp(json.dumps(answer_message))

async def main():
    await register_with_signaling()

    pc = await create_peer_connection()

    await send_answer()

    while True:
        pass  # Implement further logic as needed

if __name__ == "__main__":
    asyncio.run(main())